pragma solidity ^0.4.11;

/*
This is an example contract that helps test the functionality of the approveAndCall() functionality of HumanStandardToken.sol.
This one assumes successful receival of approval.
*/
contract SampleRecipientSuccess {
  /* A Generic receiving function for contracts that accept tokens */
  address public from;
  uint256 public value;


  event ReceivedApproval(uint256 _value);

  function receiveApproval(address _from, uint256 _value) {
    from = _from;
    value = _value;
    ReceivedApproval(_value);
  }

  function getPublic() constant returns(uint256) {
    return value;
  }
}