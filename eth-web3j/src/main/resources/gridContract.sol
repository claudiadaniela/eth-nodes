pragma solidity ^0.4.11;

contract GridContract {


    int[] public incentivePrice = new int[](24);
    int[] public finePrice = new int[](24);
    int[] public energyPrice = new int[](24);

    int[] public imbalancesProfile = new int[](24);

    event DRSignalRegistration(address from, int[] Erequest);
    event ImbalanceRegistration(address from, int[] imbalance);




    function verifyDrRequest(int[] Erequest, uint h1, uint h2) constant returns (int[]) {
        int[] memory DrSignal = new int[](Erequest.length);

        // daca nu se face bine call-ul?
        if(Erequest.length != (h2 - h1)){
            return DrSignal;
        }

        uint counter = 0;
        for(uint i = h1; i < h2; i++){
            if(imbalancesProfile[i] != 0 && sign(Erequest[counter]) != sign(imbalancesProfile[i])){
                DrSignal[counter] = findMinimum(abs(Erequest[counter]), abs(imbalancesProfile[i])) * sign(Erequest[counter]);
            } else {
                DrSignal[counter] = 0;
            }

            counter++;
        }

        return DrSignal;
    }



    function registerDrRequest(int[] Erequest, uint h1, uint h2) returns (int[]){
        int[] memory DrSignal = verifyDrRequest(Erequest, h1, h2);

        uint cnt = 0;
        for (uint i = h1; i < h2; i++) {
            imbalancesProfile[i] += DrSignal[cnt];
            cnt++;
        }
        DRSignalRegistration(tx.origin, DrSignal);
        return DrSignal;
    }


    function registerImbalance(int[] imbalance, uint h1, uint h2) {
        uint cnt = 0;
        for (uint i = h1; i < h2; i++) {
            imbalancesProfile[i] += imbalance[cnt];
            cnt++;
        }
        ImbalanceRegistration(tx.origin,  imbalance);
    }


    function computeEnergyPrice(int[] DRsignal, int[] Ebaseline, int[] Emonitored, uint h1, uint h2) constant returns (int[]) {
        uint length = h2 - h1;
        int[] memory Eoffset = new int[](length);
        int[] memory Eincentive = new int[](length);
        int[] memory Etax = new int[](length);
        int[] memory Ebill = new int[](length);

        for(uint i = 0; i < length; i++){
            Eoffset[i] = Emonitored[i] - Ebaseline[i];
            Eincentive[i] = findMinimum(Eoffset[i], DRsignal[i]) * incentivePrice[i];
            Etax[i] = abs(Eoffset[i] - DRsignal[i]) * finePrice[i];
            Ebill[i] = (Emonitored[i] * energyPrice[i]) -  Eincentive[i] + Etax[i];
        }

        return Ebill;
    }

    function registerClientBaseline(int[] baseline) {

        for (uint i = 0; i < baseline.length; i++) {
            imbalancesProfile[i] += baseline[i];
        }
    }

  function resetBaseline() {
        for (uint i = 0; i < imbalancesProfile.length; i++) {
            imbalancesProfile[i] =0;
        }
    }

   function registerFinePrice(int[] price) {
        if(price.length >24){
            return ;
        }

        for (uint i = 0; i < price.length; i++) {
            finePrice[i] = price[i];
        }
    }


   function registerIncentivePrice(int[] price) {
        if(price.length >24){
            return ;
        }

        for (uint i = 0; i < price.length; i++) {
            incentivePrice[i] = price[i];
        }
    }

    function registerEnergyPrice(int[] price) {
        if(price.length >24){
            return ;
        }

        for (uint i = 0; i < price.length; i++) {
            energyPrice[i] = price[i];
        }
    }

    function getImbalances() constant returns (int[]) {
        return imbalancesProfile;
    }

    function getFinePrice() constant returns (int[]) {
        return finePrice;
    }

     function getIncentivePrice() constant returns (int[]) {
        return incentivePrice;
     }

     function getEnergyPrice() constant returns (int[]) {
        return energyPrice;
     }

    function sign(int number) private constant returns(int){
        int result = 0;

        if(number < 0){
            result = -1;
        } else if(number > 0){
            result = 1;
        }

        return result;
    }

    function findMinimum(int number1, int number2) private constant returns (int)  {
        int min = number1;

        if(number2 < number1){
            min = number2;
        }

        return min;
    }


    function abs(int number) constant private returns (int) {
        int result = number;

        if(number < 0){
            result *= (-1);
        }

        return result;
    }
}