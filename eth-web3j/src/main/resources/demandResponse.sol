pragma solidity ^0.4.11;


contract GridContract {


    int256[] public incentivePrice = new int256[](24);

    int256[] public finePrice = new int256[](24);

    int256[] public energyPrice = new int256[](24);

    int256[] public imbalancesProfile = new int256[](24);

    event DRSignalRegistration(address from, int256[] Erequest);

    event ImbalanceRegistration(address from, int256[] imbalance);


    function verifyDrRequest(int256 value, uint h) constant returns (int256){
        int256 drSignal = 0;
        if (imbalancesProfile[h] != 0 && sign(value) != sign(imbalancesProfile[h])) {
            drSignal = findMinimum(abs(value), abs(imbalancesProfile[h])) * sign(value);
        }
        else {
            drSignal = 0;
        }
        return drSignal;
    }

    function verifyDrRequest(int256[] Erequest, uint h1, uint h2) constant returns (int256[]) {
        int256[] memory DrSignal = new int256[](Erequest.length);

        // daca nu se face bine call-ul?
        if (Erequest.length != (h2 - h1)) {
            return DrSignal;
        }

        uint counter = 0;
        for (uint i = h1; i < h2; i++) {
            DrSignal[counter] = verifyDrRequest(Erequest[counter], i);
            counter++;
        }
        return DrSignal;
    }

    function registerDrRequest(int256 value, uint h)returns (int256){
        int256 drSignal = verifyDrRequest(value, h);
        imbalancesProfile[h] += drSignal;
        return drSignal;
    }

    function registerDrRequest(int256[] Erequest, uint h1, uint h2) {
        int256 [] memory DrSignal = verifyDrRequest(Erequest, h1, h2);

        uint cnt = 0;
        for (uint i = h1; i < h2; i++) {
            imbalancesProfile[i] += DrSignal[cnt];
            cnt++;
        }
        DRSignalRegistration(tx.origin, DrSignal);
    }

    function registerImbalance(int256 value, uint h){
        imbalancesProfile[h] += value;
    }

    function registerImbalance(int256[] imbalance, uint h1, uint h2) {
        uint cnt = 0;
        for (uint i = h1; i < h2; i++) {
            imbalancesProfile[i] += imbalance[cnt];
            cnt++;
        }
        ImbalanceRegistration(tx.origin, imbalance);
    }


    function computeEnergyPrice(int256[] DRsignal, int256[] Ebaseline, int256[] Emonitored, uint h1, uint h2) constant returns (int256[]) {
        uint length = h2 - h1;
        int256[] memory Eoffset = new int256[](length);
        int256[] memory Eincentive = new int256[](length);
        int256[] memory Etax = new int256[](length);
        int256[] memory Ebill = new int256[](length);

        for (uint i = 0; i < length; i++) {
            Eoffset[i] = Emonitored[i] - Ebaseline[i];
            Eincentive[i] = findMinimum(Eoffset[i], DRsignal[i]) * incentivePrice[i];
            Etax[i] = abs(Eoffset[i] - DRsignal[i]) * finePrice[i];
            Ebill[i] = (Emonitored[i] * energyPrice[i]) - Eincentive[i] + Etax[i];
        }

        return Ebill;
    }

    function registerClientBaseline(int256[] baseline) {

        for (uint i = 0; i < baseline.length; i++) {
            imbalancesProfile[i] += baseline[i];
        }
    }

    function resetBaseline() {
        for (uint i = 0; i < imbalancesProfile.length; i++) {
            imbalancesProfile[i] = 0;
        }
    }

    function registerFinePrice(int256[] price) {
        if (price.length > 24) {
            return;
        }

        for (uint i = 0; i < price.length; i++) {
            finePrice[i] = price[i];
        }
    }


    function registerIncentivePrice(int256[] price) {
        if (price.length > 24) {
            return;
        }

        for (uint i = 0; i < price.length; i++) {
            incentivePrice[i] = price[i];
        }
    }

    function registerEnergyPrice(int256[] price) {
        if (price.length > 24) {
            return;
        }

        for (uint i = 0; i < price.length; i++) {
            energyPrice[i] = price[i];
        }
    }

    function getImbalances() constant returns (int256[]) {
        return imbalancesProfile;
    }

    function getFinePrice() constant returns (int256[]) {
        return finePrice;
    }

    function getIncentivePrice() constant returns (int256[]) {
        return incentivePrice;
    }

    function getEnergyPrice() constant returns (int256[]) {
        return energyPrice;
    }

    function sign(int256 number) private constant returns (int256){
        int256 result = 0;

        if (number < 0) {
            result = - 1;
        }
        else if (number > 0) {
            result = 1;
        }

        return result;
    }

    function findMinimum(int256 number1, int256 number2) private constant returns (int256)  {
        int256 min = number1;

        if (number2 < number1) {
            min = number2;
        }

        return min;
    }


    function abs(int256 number) constant private returns (int256) {
        int256 result = number;

        if (number < 0) {
            result *= (- 1);
        }

        return result;
    }
}


contract OwnerContract {

    address public gridContractAddress;

    int256[] public baseline = new int256[](24);

    int256[] public drSignal = new int256[](24);

    struct MonitoredValue {
    uint time;
    int256 value;
    }

    mapping (uint => MonitoredValue) public values;

    event MonitoredValueEvent(uint _time, int256 _value);

    event DRSignalRegistration(int256[] Erequest);

    event ImbalanceRegistration(int256[] imbalance);

    function init(address a){
        gridContractAddress = a;
    }

    function monitorValue(uint _time, int256 _value)
    {
        values[_time].value = values[_time].value + _value;
        MonitoredValueEvent(_time, _value);
    }

    function updateBaseline(int256[] consumptionPattern, int256[] productionForecast) {
        if (consumptionPattern.length > 24 ||
        productionForecast.length > 24 ||
        productionForecast.length != consumptionPattern.length) {
            return;
        }
        for (uint i = 0; i < productionForecast.length; i++) {
            baseline[i] = consumptionPattern[i] + productionForecast[i];
        }

        GridContract c = GridContract(gridContractAddress);
        c.registerClientBaseline(baseline);
    }


    function updateStateIntraDay(int256[] consumptionForecast, int256[] productionForecast, uint h1, uint h2) {

        if (consumptionForecast.length != (h2 - h1) || consumptionForecast.length != productionForecast.length) {
            return;
        }

        int256[] memory forecast = new int256[](h2 - h1);
        int256[] memory imbalance = new int256[](h2 - h1);
        GridContract c = GridContract(gridContractAddress);
        int256[] memory drSignalTemp = new int256[](h2 - h1);

        for (uint i = 0; i < (h2 - h1); i++) {
            forecast[i] = consumptionForecast[i] + productionForecast[i];
            imbalance[i] = forecast[i] - baseline[h1 + i];
            drSignalTemp[i] = c.registerDrRequest( imbalance[i], h1+i);

            drSignal[h1 + i] = drSignal[h1 + i] + drSignalTemp[i];
            imbalance[i] = imbalance[i] - drSignal[h1 + i];
        }

        c.registerImbalance(imbalance, h1, h2);
        DRSignalRegistration(drSignalTemp);
        ImbalanceRegistration(imbalance);
    }

    function registerDRSignal(int256[] drRequest, uint h1, uint h2){
        if (drRequest.length != (h2 - h1)) {
            return;
        }

        GridContract c = GridContract(gridContractAddress);
        int256[] memory drSignalTemp = new int256[](h2 - h1);

        for (uint i = 0; i < (h2 - h1); i++) {
            drSignalTemp[i] = c.registerDrRequest( drRequest[i], h1+i);
            drSignal[h1 + i] = drSignal[h1 + i] + drSignalTemp[i];
        }
        DRSignalRegistration(drSignalTemp);
    }

    function resetBaseline() {
        for (uint i = 0; i < baseline.length; i++) {
            baseline[i] = 0;
        }
    }

    function getBaseline() constant returns (int256[]) {
        return baseline;
    }

    function getDrSignal() constant returns (int256[]) {
        return drSignal;
    }

    function getGridAddress() constant returns (address) {
        return gridContractAddress;
    }
}


contract DeviceContract {

    address public clientAddress;

    uint lastMonitoredTime;

    int256 lastMonitoredValue;

    function init(address a){
        clientAddress = a;
    }

    function updateValue(uint time, int256 value) {
        lastMonitoredTime = time;
        lastMonitoredValue = value;
        OwnerContract c = OwnerContract(clientAddress);
        c.monitorValue(time, value);
    }

    function getClientAddress() constant returns (address) {
        return clientAddress;
    }
}