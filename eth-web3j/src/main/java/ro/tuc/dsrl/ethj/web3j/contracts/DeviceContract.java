package ro.tuc.dsrl.ethj.web3j.contracts;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Future;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;

/**
 * Auto generated code.<br>
 * <strong>Do not modify!</strong><br>
 * Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>, or {@link org.web3j.codegen.SolidityFunctionWrapperGenerator} to update.
 *
 * <p>Generated with web3j version 2.2.2.
 */
public final class DeviceContract extends Contract {
    private static final String BINARY = "6060604052341561000c57fe5b5b6101e98061001c6000396000f300606060405263ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166319ab453c811461005b5780632c2ab5ea146100795780639ea6b4f4146100a5578063da67c3aa146100d1575bfe5b341561006357fe5b610077600160a060020a03600435166100e9565b005b341561008157fe5b610089610115565b60408051600160a060020a039092168252519081900360200190f35b34156100ad57fe5b610089610124565b60408051600160a060020a039092168252519081900360200190f35b34156100d957fe5b610077600435602435610134565b005b6000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0383161790555b50565b600054600160a060020a031681565b600054600160a060020a03165b90565b6001829055600281905560008054604080517f3f937c0d00000000000000000000000000000000000000000000000000000000815260048101869052602481018590529051600160a060020a03909216928392633f937c0d92604480820193929182900301818387803b15156101a657fe5b6102c65a03f115156101b457fe5b5050505b5050505600a165627a7a7230582069183dd81f629224db24692358cc02a95115b45cd29e25b5c939005bc5a1cf140029";

    private DeviceContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private DeviceContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public Future<TransactionReceipt> init(Address a) {
        Function function = new Function("init", Arrays.<Type>asList(a), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<Address> clientAddress() {
        Function function = new Function("clientAddress", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Address> getClientAddress() {
        Function function = new Function("getClientAddress", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> updateValue(Uint256 time, Int256 value) {
        Function function = new Function("updateValue", Arrays.<Type>asList(time, value), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public static Future<DeviceContract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(DeviceContract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static Future<DeviceContract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(DeviceContract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static DeviceContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new DeviceContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static DeviceContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new DeviceContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }
}
