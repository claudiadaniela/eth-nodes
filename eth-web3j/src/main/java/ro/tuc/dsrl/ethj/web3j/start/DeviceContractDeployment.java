package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Int8;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import ro.tuc.dsrl.ethj.web3j.contracts.DeviceContract;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


public class DeviceContractDeployment {


    private DeviceContract contract;
    private Web3j web3j;

    public DeviceContractDeployment(Web3j web3j){
        this.web3j = web3j;

    }

    public void loadContract(Credentials credentials, String deviceContractAddress){
        contract =  DeviceContract.load(
                deviceContractAddress, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }
    public void deployContract(Credentials credentials)
            throws ExecutionException, InterruptedException, IOException {

        Future<DeviceContract> contractFuture = DeviceContract.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT, BigInteger.ZERO);

         contract =  contractFuture.get();
        System.out.println("========= DEVICE Contract ========");
        System.out.println("address: "+ contract.getContractAddress());
        System.out.println("gas price: "+ contract.getGasPrice());

    }

    public void init(String ownerContractAddress) throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.init(new Address(ownerContractAddress));
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= DEVICE receiveApproval ========");
        System.out.println("TX init hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX init block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX init gas: "+incrementalTransaction.getGasUsed());
    }

    public void getAddress( ) throws ExecutionException, InterruptedException {
        Future<Address> futureValue= contract.getClientAddress();
        Address value=  futureValue.get();
        System.out.println("========= DEVICE address ========");
        System.out.println("Call public get address method : "+value);
    }

    public void updateValue( ) throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.updateValue(new Uint256(1), new Int256(13));
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= DEVICE updateValue ========");
        System.out.println("TX updateValue hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX updateValue block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX updateValue gas: "+incrementalTransaction.getGasUsed());
    }

}
