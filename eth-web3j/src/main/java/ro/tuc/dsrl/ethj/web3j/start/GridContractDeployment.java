package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Int8;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import ro.tuc.dsrl.ethj.web3j.contracts.GridContract;
import ro.tuc.dsrl.ethj.web3j.contracts.OwnerContract;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Claudia-PC on 6/9/2017.
 */
public class GridContractDeployment {


    private Web3j web3j;
    private GridContract contract;

    public GridContractDeployment(Web3j web3j){
        this.web3j = web3j;

    }

    public void  loadContract(Credentials credentials, String ownerContractAddress){
        contract = GridContract.load(
                ownerContractAddress, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }
    public void deployContract( Credentials credentials)
            throws ExecutionException, InterruptedException, IOException {

        Future<GridContract> contractFuture = GridContract.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT, BigInteger.ZERO);

        contract =  contractFuture.get();

        System.out.println("========= GRID Contract ========");
        System.out.println("address: "+ contract.getContractAddress());
        System.out.println("gas price: "+ contract.getGasPrice());

    }

    public void verifyDrRequest() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(10));
        values.add(new Int256(11));
        values.add(new Int256(12));
        values.add(new Int256(13));

        DynamicArray<Int256> Erequest = new DynamicArray<>(values);

        Uint256 h1 = new Uint256(0);
        Uint256 h2 = new Uint256(4);

        Future<DynamicArray<Int256>> returnedValues= contract.verifyDrRequest(Erequest, h1, h2);
        DynamicArray<Int256> results =  returnedValues.get();
        System.out.println("========= GRID verifyDrRequest ========");
        for(Int256 r: results.getValue()) {
            System.out.println("validated DR : " + r.getValue());

        }
    }

    public void registerImbalance() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(10));
        values.add(new Int256(9));
        values.add(new Int256(-13));
        values.add(new Int256(-2));

        DynamicArray<Int256> Erequest = new DynamicArray<>(values);

        Uint256 h1 = new Uint256(0);
        Uint256 h2 = new Uint256(4);
        Future<TransactionReceipt> futureTranscationReceipt= contract.registerImbalance(Erequest, h1, h2);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID registerImbalance ========");
        System.out.println("TX registerImbalance hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerImbalance block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerImbalance gas: "+incrementalTransaction.getGasUsed());

        checkImbalancesEvents( incrementalTransaction);

    }
    public void registerDRRequest() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(10));
        values.add(new Int256(11));
        values.add(new Int256(12));
        values.add(new Int256(13));

        DynamicArray<Int256> Erequest = new DynamicArray<>(values);

        Uint256 h1 = new Uint256(0);
        Uint256 h2 = new Uint256(4);

        Future<TransactionReceipt> futureTranscationReceipt= contract.registerDrRequest(Erequest, h1, h2);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID registerDrRequest ========");
        System.out.println("TX registerDrRequest hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerDrRequest block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerDrRequest gas: "+incrementalTransaction.getGasUsed());

        checkDRRequestsEvents( incrementalTransaction);

    }
    public void checkImbalancesEvents(  TransactionReceipt receipt ){
        List<GridContract.ImbalanceRegistrationEventResponse> events =contract.getImbalanceRegistrationEvents(receipt);
        System.out.println("========= GRID imbalances events ========");
        for(GridContract.ImbalanceRegistrationEventResponse resp : events){
            System.out.println("registered imbalances : from:"+ resp.from + " values: " );
            for(Int256 v: resp.imbalance.getValue()){
                System.out.print( v.getValue() + " ");
            }
            System.out.println();
        }
    }


    public void checkDRRequestsEvents(  TransactionReceipt receipt ){
        List<GridContract.DRSignalRegistrationEventResponse> events =contract.getDRSignalRegistrationEvents(receipt);
        System.out.println("========= GRID dr requests events ========");
        for(GridContract.DRSignalRegistrationEventResponse resp : events){
            System.out.println("registered dr : from:"+ resp.from + " values: " );
            for(Int256 v: resp.Erequest.getValue()){
                System.out.print( v.getValue() + " ");
            }
            System.out.println();
        }
    }



    public void getGridImbalances() throws ExecutionException, InterruptedException {
        Future<DynamicArray<Int256>>  futureValue= contract.getImbalances();
        DynamicArray<Int256> values=  futureValue.get();
        System.out.println("========= GRID imbalances state ========");
        for(Int256 v : values.getValue()) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();
    }

    public void resetBaseline() throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.resetBaseline();
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID resetBaseline ========");
        System.out.println("TX resetBaseline hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX resetBaseline block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX resetBaseline gas: "+incrementalTransaction.getGasUsed());
    }

    public void registerEnergyPrice() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        Random rand = new Random();
        for(int i =0;i<24;i++){
              values.add(new Int256(rand.nextInt(50) + 1));
        }

        DynamicArray<Int256> price = new DynamicArray<>(values);


        Future<TransactionReceipt> futureTranscationReceipt= contract.registerEnergyPrice(price);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID register Energy Price ========");
        System.out.println("TX registerEnergyPrice hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerEnergyPrice block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerEnergyPrice gas: "+incrementalTransaction.getGasUsed());

        Future<DynamicArray<Int256>>  futureValue= contract.getEnergyPrice();
        DynamicArray<Int256> valuesPrrice=  futureValue.get();
        System.out.println("========= GRID energy price state ========");
        for(Int256 v : valuesPrrice.getValue()) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();

    }

    public void registerFinePrice() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        Random rand = new Random();
        for(int i =0;i<24;i++){
            values.add(new Int256(rand.nextInt(50) + 1));
        }

        DynamicArray<Int256> price = new DynamicArray<>(values);


        Future<TransactionReceipt> futureTranscationReceipt= contract.registerFinePrice(price);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID register FINE Price ========");
        System.out.println("TX registerFinePrice hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerFinePrice block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerFinePrice gas: "+incrementalTransaction.getGasUsed());

        Future<DynamicArray<Int256>>  futureValue= contract.getFinePrice();
        DynamicArray<Int256> valuesPrrice=  futureValue.get();
        System.out.println("========= GRID fine price state ========");
        for(Int256 v : valuesPrrice.getValue()) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();

    }

    public void registerIncentivePrice() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        Random rand = new Random();
        for(int i =0;i<24;i++){
            values.add(new Int256(rand.nextInt(50) + 1));
        }

        DynamicArray<Int256> price = new DynamicArray<>(values);


        Future<TransactionReceipt> futureTranscationReceipt= contract.registerIncentivePrice(price);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= GRID register FINE Price ========");
        System.out.println("TX registerIncentivePrice hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerIncentivePrice block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerIncentivePrice gas: "+incrementalTransaction.getGasUsed());

        Future<DynamicArray<Int256>>  futureValue= contract.getIncentivePrice();
        DynamicArray<Int256> valuesPrrice=  futureValue.get();
        System.out.println("========= GRID incentive price state ========");
        for(Int256 v : valuesPrrice.getValue()) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();

    }


    public void computePrice() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(10));
        values.add(new Int256(11));
        values.add(new Int256(12));
        values.add(new Int256(13));

        DynamicArray<Int256> dr = new DynamicArray<>(values);

        values = new ArrayList<>();
        values.add(new Int256(4));
        values.add(new Int256(7));
        values.add(new Int256(4));
        values.add(new Int256(2));

        DynamicArray<Int256> baseline = new DynamicArray<>(values);

        values = new ArrayList<>();
        values.add(new Int256(14));
        values.add(new Int256(17));
        values.add(new Int256(34));
        values.add(new Int256(2));

        DynamicArray<Int256> monitored = new DynamicArray<>(values);

        Uint256 h1 = new Uint256(0);
        Uint256 h2 = new Uint256(4);

        Future<DynamicArray<Int256>> valueFuture= contract.computeEnergyPrice(dr, baseline,monitored, h1, h2);
        DynamicArray<Int256> valuesReturned=  valueFuture.get();

        System.out.println("========= GRID computed price  ========");
        for(Int256 v : valuesReturned.getValue()) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();

    }

    public String getContractAddres(){
        return contract.getContractAddress();
    }
}
