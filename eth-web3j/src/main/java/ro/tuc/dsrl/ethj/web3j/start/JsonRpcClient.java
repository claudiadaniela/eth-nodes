package ro.tuc.dsrl.ethj.web3j.start;


import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;
import org.web3j.abi.datatypes.Utf8String;
import ro.tuc.dsrl.ethj.web3j.contracts.Sample;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/*

* start geth node with -rpc in order to allow connections
* https://docs.web3j.io/smart_contracts.html#smart-contract-wrappers
 */
public class JsonRpcClient {

    private static String miner = "0x3b7b795f097bc6f131a4ded575d0976fc6a6a83e"; //"aaa" node1
    private static String regular = "0x8be71dee421d4e272f5f69abde9de193d396a8ec";// "bbb" node2
    private static   BigInteger GAS_PRICE =  BigInteger.valueOf(20_000_000_000L);//Convert.toWei("1", Convert.Unit.ETHER).toBigInteger();

    private static   BigInteger GAS_LIMIT = BigInteger.valueOf(4_300_000);//Convert.toWei("10", Convert.Unit.ETHER).toBigInteger();

    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException, CipherException {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101/"));  // defaults to http://localhost:8545/
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);



        Credentials credentials = WalletUtils.loadCredentials("aaa", "E:/projects/blockchain/gethCluster/node1/keystore/UTC--2017-06-06T13-54-07.614832900Z--3b7b795f097bc6f131a4ded575d0976fc6a6a83e");

        EthGetBalance ethGetBalance = web3j.ethGetBalance(
                miner, DefaultBlockParameter.valueOf("latest")).send();
        System.out.println("========= Balance========");
        System.out.println("id: "+ ethGetBalance.getId());
        System.out.println("error: "+ ethGetBalance.getError());
        System.out.println("result: "+ ethGetBalance.getBalance() );



//        try {
//            TransactionReceipt transactionReceipt = Transfer.sendFunds(
//                    web3j, credentials, regular, BigDecimal.valueOf(1.0), Convert.Unit.ETHER);
//
//            System.out.println("========= TX========");
//            System.out.println("block: "+ transactionReceipt.getBlockHash());
//            System.out.println("hash: "+ transactionReceipt.getTransactionHash());
//            System.out.println("from: "+ transactionReceipt.getFrom());
//            System.out.println("to: "+ transactionReceipt.getTo());
//        } catch (TransactionTimeoutException e) {
//            e.printStackTrace();
//        }
//
//
//        System.out.println("Completed");


//
//        BigInteger value = Convert.toWei("1", Convert.Unit.ETHER).toBigInteger();
//
//        Future<Sample> contractFuture = Sample.deploy(web3j, credentials, GAS_PRICE, GAS_LIMIT, value);
//
//        Sample contract = contractFuture.get();
//
//        System.out.println("========= Contract ========");
//        System.out.println("address: "+ contract.getContractAddress());
//        System.out.println("gas price: "+ contract.getGasPrice());
//
//        Optional<TransactionReceipt> futureReceipt = contract.getTransactionReceipt();
//
//        System.out.println("Contract Receipt: " + futureReceipt.get().getTransactionHash()+ "  Block : " + futureReceipt.get().getBlockNumber() + " gas used: " + futureReceipt.get().getGasUsed() + " contract address: " + futureReceipt.get().getContractAddress());

       Sample   contract = Sample.load(
               "0x0ef15bb4e2026309afc786d34bd5ec7b6e72720c", web3j, credentials, GAS_PRICE, GAS_LIMIT);
//0x6c8d3e3ee4cbbe280ac275c0b2f7d2e62d5c3c714e5b9b17b2a8702aa5e31425

//        System.out.println("Contract is valid: "+ contract.isValid());
//
        Future<TransactionReceipt> futureTranscationReceipt1 =  contract.inc(new Int256(7));
        TransactionReceipt incrementalTransaction1=  futureTranscationReceipt1.get();
        System.out.println("TX inc hash : "+incrementalTransaction1.getTransactionHash());
        System.out.println("TX inc block: "+incrementalTransaction1.getBlockNumber());
        System.out.println("TX inc gas: "+incrementalTransaction1.getGasUsed());

        List<Sample.SentEventResponse> sentEventResponses = contract.getSentEvents(incrementalTransaction1);
        for(Sample.SentEventResponse resp : sentEventResponses){
            System.out.println("INC from: "+ resp.from);
            System.out.println("INC n: "+resp.n);
        }

//
//        Future<TransactionReceipt> futureTranscationReceipt2 =  contract.obtainValueFixed();
//        TransactionReceipt incrementalTransaction2=  futureTranscationReceipt2.get();
//        System.out.println("TX string hash : "+incrementalTransaction2.getTransactionHash());
//        System.out.println("TX string block: "+incrementalTransaction2.getBlockNumber());
//        System.out.println("TX string gas: "+incrementalTransaction2.getGasUsed());
//        System.out.println("TX string : "+incrementalTransaction2.toString());

//
//        Sample contract = Sample.load(
//                "0xb7a43579a189304dbb48af382500378853f5d63e", web3j, credentials, GAS_PRICE, GAS_LIMIT);
//        System.out.println("Contract is valid: "+ contract.isValid());
//
//        Future<Int256> futureTranscationReceipt2 =  contract.obtainValue();
//        Int256 incrementalTransaction2=  futureTranscationReceipt2.get();
//        System.out.println("TX2 get: "+incrementalTransaction2);
//////0x198697d5fee6b04c3db16980dfad5d8228b4f128
////
//        Future<Bool> futureTranscationReceipt3 =  contract.isDeployed();
//        Bool incrementalTransaction3=  futureTranscationReceipt3.get();
//        System.out.println("TX3 get: "+incrementalTransaction3);

//        Sample contract = Sample.load(
//                "0x92b6fe5f85bcb9d25c4a0d63dc22c4f7c6098d63", web3j, credentials, GAS_PRICE, GAS_LIMIT);
//        Future<Utf8String> futureTranscationReceipt5 =  contract.getStringValue();
//        Utf8String incrementalTransaction5=  futureTranscationReceipt5.get();
//        System.out.println("TX String get: "+incrementalTransaction5);
//
//           contract = Sample.load(
//                "0x2da10c507c1cd6606d1713a6647e8c2d9924901c", web3j, credentials, GAS_PRICE, GAS_LIMIT);
//        Future<Int256> futureTranscationReceipt4 =  contract.obtainValue();
//        Int256 incrementalTransaction4=  futureTranscationReceipt4.get();
//        System.out.println("TX4 get: "+incrementalTransaction4);
//


//        Function function = new Function("obtainValueFixed", Arrays.<Type>asList(), Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
//        try {
//            System.out.println( callSmartContractFunction(function, contract.getContractAddress(), web3j ));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        obtainFixedValue(web3j, contract.getContractAddress());
        //isDeployedCall(web3j, contract.getContractAddress());
    }

    private static void isDeployedCall( Web3j web3j,String contractAddress) throws ExecutionException, InterruptedException {


        Function function = new Function("isDeployed",
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        String functionEncoder = FunctionEncoder.encode(function);
        EthCall response = web3j.ethCall(
                org.web3j.protocol.core.methods.request.Transaction.createEthCallTransaction(miner,
                        contractAddress, functionEncoder
                ),DefaultBlockParameterName.LATEST).sendAsync().get();
        List<Type> someType = FunctionReturnDecoder.decode(response.getValue(),function.getOutputParameters());
        Iterator<Type> it = someType.iterator();
        Type resault = someType.get(0);
        String a = resault.toString();
        System.out.println(a);
    }
    private static void obtainFixedValue( Web3j web3j,String contractAddress) throws ExecutionException, InterruptedException {

        List<Type> inputParameters = new ArrayList<>();
        List<TypeReference<?>> outputParameters = Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {});
        Function function = new Function("obtainValueFixed",
                inputParameters,
                outputParameters);
        String functionEncoder = FunctionEncoder.encode(function);
        EthSendTransaction response = web3j.ethSendTransaction(
                org.web3j.protocol.core.methods.request.Transaction.createEthCallTransaction(miner,
                        contractAddress, functionEncoder
                )).sendAsync().get();
//        List<Type> someType = FunctionReturnDecoder.decode(response.getResult(),function.getOutputParameters());
//        Iterator<Type> it = someType.iterator();
//        Type resault = someType.get(0);
//        String a = resault.toString();
        System.out.println(response.getResult());
    }


    private static String callSmartContractFunction2(
            Function function, String contractAddress, Web3j web3j) throws Exception {


        EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                miner, DefaultBlockParameterName.LATEST).sendAsync().get();
        BigInteger nonce = ethGetTransactionCount.getTransactionCount();

        String encodedFunction = FunctionEncoder.encode(function);
        System.out.println("Encoded function- data: " +  Numeric.prependHexPrefix(encodedFunction));


        org.web3j.protocol.core.methods.request.Transaction trans = org.web3j.protocol.core.methods.request.Transaction.createFunctionCallTransaction(
        miner, nonce, GAS_PRICE, GAS_LIMIT, contractAddress,
        Convert.toWei("1", Convert.Unit.ETHER).toBigInteger(), encodedFunction);

                org.web3j.protocol.core.methods.response.EthSendTransaction response = web3j.ethSendTransaction(trans)
                .sendAsync().get();


        List<Type> someTypes = FunctionReturnDecoder.decode(
                response.getResult(), function.getOutputParameters());
        for(Type t : someTypes){
            System.out.println(t);
        }
        return response.getResult();
    }

    private BigInteger estimateGas(Web3j web3j, String encodedFunction) throws Exception {
        EthEstimateGas ethEstimateGas = web3j.ethEstimateGas(
                org.web3j.protocol.core.methods.request.Transaction.createEthCallTransaction(miner, null, encodedFunction))
                .sendAsync().get();
        // this was coming back as 50,000,000 which is > the block gas limit of 4,712,388
        // see eth.getBlock("latest")
        return ethEstimateGas.getAmountUsed().divide(BigInteger.valueOf(100));
    }
    private void createFilterForEvent(Web3j web3j,
            String encodedEventSignature, String contractAddress) throws Exception {
        org.web3j.protocol.core.methods.request.EthFilter ethFilter = new org.web3j.protocol.core.methods.request.EthFilter(
                DefaultBlockParameterName.EARLIEST,
                DefaultBlockParameterName.LATEST,
                contractAddress
        );


        web3j.ethLogObservable(ethFilter).subscribe(log -> {
            System.out.println(log);
        });

    }
}
