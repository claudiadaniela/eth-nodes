package ro.tuc.dsrl.ethj.web3j.contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * Auto generated code.<br>
 * <strong>Do not modify!</strong><br>
 * Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>, or {@link org.web3j.codegen.SolidityFunctionWrapperGenerator} to update.
 *
 * <p>Generated with web3j version 2.2.2.
 */
public final class SampleRecipientSuccess extends Contract {
    private static final String BINARY = "6060604052341561000c57fe5b5b6101ce8061001c6000396000f300606060405263ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416632c044779811461005b5780633fa4f2451461007d5780634bdf1b0e1461009f578063d5ce3389146100cd575bfe5b341561006357fe5b61006b610106565b60408051918252519081900360200190f35b341561008557fe5b61006b61010d565b60408051918252519081900360200190f35b34156100a757fe5b6100cb73ffffffffffffffffffffffffffffffffffffffff60043516602435610113565b005b34156100d557fe5b6100dd610186565b6040805173ffffffffffffffffffffffffffffffffffffffff9092168252519081900360200190f35b6001545b90565b60015481565b6000805473ffffffffffffffffffffffffffffffffffffffff191673ffffffffffffffffffffffffffffffffffffffff841617905560018190556040805182815290517f2db24179b782aab7c5ab64add7f84d4f6c845d0779695371f29be1f658d043cd916020908290030190a15b5050565b60005473ffffffffffffffffffffffffffffffffffffffff16815600a165627a7a72305820cc48a40818c320afb2026f47cb8858e25d639006c61c036d5e9ebed623050b820029";

    private SampleRecipientSuccess(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private SampleRecipientSuccess(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<ReceivedApprovalEventResponse> getReceivedApprovalEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("ReceivedApproval", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<ReceivedApprovalEventResponse> responses = new ArrayList<ReceivedApprovalEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            ReceivedApprovalEventResponse typedResponse = new ReceivedApprovalEventResponse();
            typedResponse._value = (Uint256) eventValues.getNonIndexedValues().get(0);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ReceivedApprovalEventResponse> receivedApprovalEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("ReceivedApproval", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, ReceivedApprovalEventResponse>() {
            @Override
            public ReceivedApprovalEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                ReceivedApprovalEventResponse typedResponse = new ReceivedApprovalEventResponse();
                typedResponse._value = (Uint256) eventValues.getNonIndexedValues().get(0);
                return typedResponse;
            }
        });
    }

    public Future<Uint256> getPublic() {
        Function function = new Function("getPublic", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Uint256> value() {
        Function function = new Function("value", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> receiveApproval(Address _from, Uint256 _value) {
        Function function = new Function("receiveApproval", Arrays.<Type>asList(_from, _value), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<Address> from() {
        Function function = new Function("from", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public static Future<SampleRecipientSuccess> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(SampleRecipientSuccess.class, web3j, credentials, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static Future<SampleRecipientSuccess> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(SampleRecipientSuccess.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static SampleRecipientSuccess load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new SampleRecipientSuccess(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static SampleRecipientSuccess load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new SampleRecipientSuccess(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class ReceivedApprovalEventResponse {
        public Uint256 _value;
    }
}
