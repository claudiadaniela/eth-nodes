package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import ro.tuc.dsrl.ethj.web3j.contracts.Sample;
import ro.tuc.dsrl.ethj.web3j.contracts.SampleRecipientSuccess;
import rx.Observable;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by Claudia-PC on 6/7/2017.
 */
public class SampleReciepientSuccessDeployment {

    private static String miner = "0x3b7b795f097bc6f131a4ded575d0976fc6a6a83e"; //"aaa" node1
    private static BigInteger GAS_PRICE = BigInteger.valueOf(20_000_000_000L);//Convert.toWei("1", Convert.Unit.ETHER).toBigInteger();

    private static BigInteger GAS_LIMIT = BigInteger.valueOf(4_300_000);//Convert.toWei("10", Convert.Unit.ETHER).toBigInteger();
    private static String contractAddress= "0x6f633b49444f38904d9067ca8e994e1f4c8c92f4";

    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException, CipherException {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101/"));  // defaults to http://localhost:8545/
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);
        Credentials credentials = WalletUtils.loadCredentials("aaa", "E:/projects/blockchain/gethCluster/node1/keystore/UTC--2017-06-06T13-54-07.614832900Z--3b7b795f097bc6f131a4ded575d0976fc6a6a83e");


        checkBalance(web3j);
        SampleRecipientSuccess contract = loadContract(web3j,credentials);
       // SampleRecipientSuccess contract = deployContract(web3j, credentials);
        receiveApproval(contract);
        getValue(contract);
        getPublicValue(contract);
    }


    private static void checkBalance(Web3j web3j) throws IOException {
        EthGetBalance ethGetBalance = web3j.ethGetBalance(
                miner, DefaultBlockParameter.valueOf("latest")).send();
        System.out.println("========= Balance========");
        System.out.println("id: " + ethGetBalance.getId());
        System.out.println("error: " + ethGetBalance.getError());
        System.out.println("result: " + ethGetBalance.getBalance());
    }

    private static SampleRecipientSuccess loadContract(Web3j web3j, Credentials credentials){
        return SampleRecipientSuccess.load(
                contractAddress, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }
    private static SampleRecipientSuccess deployContract(Web3j web3j,  Credentials credentials)
            throws ExecutionException, InterruptedException, IOException {

        Future<SampleRecipientSuccess> contractFuture = SampleRecipientSuccess.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT, BigInteger.ZERO);

        SampleRecipientSuccess contract =  contractFuture.get();

        System.out.println("========= Contract ========");
        System.out.println("address: "+ contract.getContractAddress());
        System.out.println("gas price: "+ contract.getGasPrice());
        return contract;
    }

    private  static void receiveApproval(SampleRecipientSuccess contract) throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.receiveApproval(new Address(BigInteger.TEN), new Uint256(10));
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= receiveApproval ========");
        System.out.println("TX receiveApproval hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX receiveApproval block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX receiveApproval gas: "+incrementalTransaction.getGasUsed());

        checkEvents(contract, incrementalTransaction);

    }

    private static void checkEvents(SampleRecipientSuccess contract,  TransactionReceipt receipt ){
        List<SampleRecipientSuccess.ReceivedApprovalEventResponse> events =contract.getReceivedApprovalEvents(receipt);
        System.out.println("========= events ========");
        for(SampleRecipientSuccess.ReceivedApprovalEventResponse resp : events){
            System.out.println("receiveApproval value: "+ resp._value.getValue());
        }
    }


    private  static void getValue(SampleRecipientSuccess contract) throws ExecutionException, InterruptedException {
        Future<Uint256> futureValue= contract.value();
        Uint256 value=  futureValue.get();
        System.out.println("========= value ========");
        System.out.println("Call receiveApproval value : "+value.getValue().intValue());
    }

    public static void getPublicValue(SampleRecipientSuccess contract) throws ExecutionException, InterruptedException {
        Future<Uint256> futureValue= contract.getPublic();
        Uint256 value=  futureValue.get();
        System.out.println("========= value ========");
        if(value!=null){
            System.out.println("Call receiveApproval Constant method value : "+value.getValue().intValue());
        }else{
        System.out.println("Call receiveApproval Constant method value : "+value);}
    }
    private static void observerEvents(SampleRecipientSuccess contract){
        Observable<SampleRecipientSuccess.ReceivedApprovalEventResponse> obs = contract.receivedApprovalEventObservable(
                DefaultBlockParameterName.EARLIEST,
                DefaultBlockParameterName.LATEST);
        //TODO: use observers?
    }
}