package ro.tuc.dsrl.ethj.web3j.contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * Auto generated code.<br>
 * <strong>Do not modify!</strong><br>
 * Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>, or {@link org.web3j.codegen.SolidityFunctionWrapperGenerator} to update.
 *
 * <p>Generated with web3j version 2.2.2.
 */
public final class OwnerContract extends Contract {
    private static final String BINARY = "606060405260186040518059106100135750595b908082528060200260200182016040525b5080516100399160019160209091019061007d565b5060186040518059106100495750595b908082528060200260200182016040525b50805161006f9160029160209091019061007d565b50341561007857fe5b6100ea565b8280548282559060005260206000209081019282156100b8579160200282015b828111156100b857825182559160200191906001019061009d565b5b506100c59291506100c9565b5090565b6100e791905b808211156100c557600081556001016100cf565b5090565b90565b610def806100f96000396000f300606060405236156100a95763ffffffff60e060020a60003504166302eabf4781146100ab57806319ab453c146100d05780633f937c0d146100ee5780635e383d21146101065780636569039b1461013257806379277b94146101c6578063890424ce146102315780638d03a4f41461028d578063ab7ab2c3146102b9578063c352710c14610346578063c954305914610358578063d5cd68da146103c3578063ecdd26f0146103e8575bfe5b34156100b357fe5b6100be600435610414565b60408051918252519081900360200190f35b34156100d857fe5b6100ec600160a060020a0360043516610437565b005b34156100f657fe5b6100ec600435602435610463565b005b341561010e57fe5b6101196004356104bb565b6040805192835260208301919091528051918290030190f35b341561013a57fe5b6100ec600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750506040805187358901803560208181028481018201909552818452989a998901989297509082019550935083925085019084908082843750949650508435946020013593506104d492505050565b005b34156101ce57fe5b6101d661093e565b604080516020808252835181830152835191928392908301918581019102808383821561021e575b80518252602083111561021e57601f1990920191602091820191016101fe565b5050509050019250505060405180910390f35b341561023957fe5b6100ec6004808035906020019082018035906020019080806020026020016040519081016040528093929190818152602001838360200280828437509496505084359460200135935061099d92505050565b005b341561029557fe5b61029d610b85565b60408051600160a060020a039092168252519081900360200190f35b34156102c157fe5b6100ec600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750506040805187358901803560208181028481018201909552818452989a998901989297509082019550935083925085019084908082843750949650610b9595505050505050565b005b341561034e57fe5b6100ec610ce5565b005b341561036057fe5b6101d6610d20565b604080516020808252835181830152835191928392908301918581019102808383821561021e575b80518252602083111561021e57601f1990920191602091820191016101fe565b5050509050019250505060405180910390f35b34156103cb57fe5b6100be600435610d7f565b60408051918252519081900360200190f35b34156103f057fe5b61029d610da2565b60408051600160a060020a039092168252519081900360200190f35b600280548290811061042257fe5b906000526020600020900160005b5054905081565b6000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0383161790555b50565b600082815260036020908152604091829020600101805484019055815184815290810183905281517f77ce1fef081439211f4346eac932e53d883d87f29e4c570847dc4882d1ecff08929181900390910190a15b5050565b6003602052600090815260409020805460019091015482565b6104dc610db1565b6104e4610db1565b60006104ee610db1565b6000868603895114158061050457508751895114155b1561050e57610933565b86860360405180591061051e5750595b908082528060200260200182016040525b5094508686036040518059106105425750595b908082528060200260200182016040525b50600054604051919550600160a060020a03169350878703908059106105765750595b908082528060200260200182016040525b509150600090505b8686038110156107885787818151811015156105a757fe5b9060200190602002015189828151811015156105bf57fe5b906020019060200201510185828151811015156105d857fe5b60209081029091010152600180548883019081106105f257fe5b906000526020600020900160005b5054858281518110151561061057fe5b9060200190602002015103848281518110151561062957fe5b602090810290910101528351600160a060020a03841690637af5dee69086908490811061065257fe5b90602001906020020151838a016000604051602001526040518363ffffffff1660e060020a0281526004018083815260200182815260200192505050602060405180830381600087803b15156106a457fe5b6102c65a03f115156106b257fe5b50506040515183519091508390839081106106c957fe5b6020908102909101015281518290829081106106e157fe5b9060200190602002015160028289018154811015156106fc57fe5b906000526020600020900160005b505401600282890181548110151561071e57fe5b906000526020600020900160005b50556002805488830190811061073e57fe5b906000526020600020900160005b5054848281518110151561075c57fe5b9060200190602002015103848281518110151561077557fe5b602090810290910101525b60010161058f565b82600160a060020a031663ae7c395e8589896040518463ffffffff1660e060020a0281526004018080602001848152602001838152602001828103825285818151815260200191508051906020019060200280838360008314610806575b80518252602083111561080657601f1990920191602091820191016107e6565b505050905001945050505050600060405180830381600087803b151561082857fe5b6102c65a03f1151561083657fe5b505060408051602080825285518183015285517f31ed14b3524a1e5cc0d861893bc74bc844754ac1aaf769af15b4040ff297eb4794508693839290830191818601910280838382156108a3575b8051825260208311156108a357601f199092019160209182019101610883565b5050509050019250505060405180910390a160408051602080825286518183015286517fe7368ef850810e5646db806ddf9a52aa84a14166f3ebdcce55a8ab888e89d6139388939283929183019181860191028083838215610920575b80518252602083111561092057601f199092019160209182019101610900565b5050509050019250505060405180910390a15b505050505050505050565b610946610db1565b600280548060200260200160405190810160405280929190818152602001828054801561099257602002820191906000526020600020905b81548152602001906001019080831161097e575b505050505090505b90565b60006109a7610db1565b8451600090858503146109b957610b7d565b600054604051600160a060020a039091169350858503908059106109da5750595b908082528060200260200182016040525b509150600090505b848403811015610aff5782600160a060020a0316637af5dee68783815181101515610a1a57fe5b906020019060200201518388016000604051602001526040518363ffffffff1660e060020a0281526004018083815260200182815260200192505050602060405180830381600087803b1515610a6c57fe5b6102c65a03f11515610a7a57fe5b5050604051518351909150839083908110610a9157fe5b602090810290910101528151829082908110610aa957fe5b906020019060200201516002828701815481101515610ac457fe5b906000526020600020900160005b5054016002828701815481101515610ae657fe5b906000526020600020900160005b50555b6001016109f3565b60408051602080825284518183015284517f31ed14b3524a1e5cc0d861893bc74bc844754ac1aaf769af15b4040ff297eb479386939283929183019181860191028083838215610b6a575b805182526020831115610b6a57601f199092019160209182019101610b4a565b5050509050019250505060405180910390a15b505050505050565b600054600160a060020a03165b90565b60006000601884511180610baa575060188351115b80610bb757508351835114155b15610bc157610cdf565b600091505b8251821015610c2b578282815181101515610bdd57fe5b906020019060200201518483815181101515610bf557fe5b9060200190602002015101600183815481101515610c0f57fe5b906000526020600020900160005b50555b600190910190610bc6565b506000546040517f385e75370000000000000000000000000000000000000000000000000000000081526020600482019081526001805460248401819052600160a060020a0390941693849363385e7537939182916044019084908015610cb157602002820191906000526020600020905b815481526020019060010190808311610c9d575b505092505050600060405180830381600087803b1515610ccd57fe5b6102c65a03f11515610cdb57fe5b5050505b50505050565b60005b600154811015610460576000600182815481101515610d0357fe5b906000526020600020900160005b50555b600101610ce8565b5b50565b610d28610db1565b600180548060200260200160405190810160405280929190818152602001828054801561099257602002820191906000526020600020905b81548152602001906001019080831161097e575b505050505090505b90565b600180548290811061042257fe5b906000526020600020900160005b5054905081565b600054600160a060020a031681565b604080516020810190915260008152905600a165627a7a72305820184f5844b768620d5b0eda12e96d881a054751ddf3bf2a50061e61505be3f74b0029";

    private OwnerContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private OwnerContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<MonitoredValueEventEventResponse> getMonitoredValueEventEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("MonitoredValueEvent", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Int256>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<MonitoredValueEventEventResponse> responses = new ArrayList<MonitoredValueEventEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            MonitoredValueEventEventResponse typedResponse = new MonitoredValueEventEventResponse();
            typedResponse._time = (Uint256) eventValues.getNonIndexedValues().get(0);
            typedResponse._value = (Int256) eventValues.getNonIndexedValues().get(1);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<MonitoredValueEventEventResponse> monitoredValueEventEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("MonitoredValueEvent", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Int256>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, MonitoredValueEventEventResponse>() {
            @Override
            public MonitoredValueEventEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                MonitoredValueEventEventResponse typedResponse = new MonitoredValueEventEventResponse();
                typedResponse._time = (Uint256) eventValues.getNonIndexedValues().get(0);
                typedResponse._value = (Int256) eventValues.getNonIndexedValues().get(1);
                return typedResponse;
            }
        });
    }

    public List<DRSignalRegistrationEventResponse> getDRSignalRegistrationEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("DRSignalRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<DRSignalRegistrationEventResponse> responses = new ArrayList<DRSignalRegistrationEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            DRSignalRegistrationEventResponse typedResponse = new DRSignalRegistrationEventResponse();
            typedResponse.Erequest = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(0);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<DRSignalRegistrationEventResponse> dRSignalRegistrationEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("DRSignalRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, DRSignalRegistrationEventResponse>() {
            @Override
            public DRSignalRegistrationEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                DRSignalRegistrationEventResponse typedResponse = new DRSignalRegistrationEventResponse();
                typedResponse.Erequest = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(0);
                return typedResponse;
            }
        });
    }

    public List<ImbalanceRegistrationEventResponse> getImbalanceRegistrationEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("ImbalanceRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<ImbalanceRegistrationEventResponse> responses = new ArrayList<ImbalanceRegistrationEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            ImbalanceRegistrationEventResponse typedResponse = new ImbalanceRegistrationEventResponse();
            typedResponse.imbalance = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(0);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ImbalanceRegistrationEventResponse> imbalanceRegistrationEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("ImbalanceRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, ImbalanceRegistrationEventResponse>() {
            @Override
            public ImbalanceRegistrationEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                ImbalanceRegistrationEventResponse typedResponse = new ImbalanceRegistrationEventResponse();
                typedResponse.imbalance = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(0);
                return typedResponse;
            }
        });
    }

    public Future<Int256> drSignal(Uint256 param0) {
        Function function = new Function("drSignal", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> init(Address a) {
        Function function = new Function("init", Arrays.<Type>asList(a), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> monitorValue(Uint256 _time, Int256 _value) {
        Function function = new Function("monitorValue", Arrays.<Type>asList(_time, _value), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<List<Type>> values(Uint256 param0) {
        Function function = new Function("values", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Int256>() {}));
        return executeCallMultipleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> updateStateIntraDay(DynamicArray<Int256> consumptionForecast, DynamicArray<Int256> productionForecast, Uint256 h1, Uint256 h2) {
        Function function = new Function("updateStateIntraDay", Arrays.<Type>asList(consumptionForecast, productionForecast, h1, h2), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> getDrSignal() {
        Function function = new Function("getDrSignal", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> registerDRSignal(DynamicArray<Int256> drRequest, Uint256 h1, Uint256 h2) {
        Function function = new Function("registerDRSignal", Arrays.<Type>asList(drRequest, h1, h2), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<Address> getGridAddress() {
        Function function = new Function("getGridAddress", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> updateBaseline(DynamicArray<Int256> consumptionPattern, DynamicArray<Int256> productionForecast) {
        Function function = new Function("updateBaseline", Arrays.<Type>asList(consumptionPattern, productionForecast), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> resetBaseline() {
        Function function = new Function("resetBaseline", Arrays.<Type>asList(), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> getBaseline() {
        Function function = new Function("getBaseline", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Int256> baseline(Uint256 param0) {
        Function function = new Function("baseline", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Address> gridContractAddress() {
        Function function = new Function("gridContractAddress", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public static Future<OwnerContract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(OwnerContract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static Future<OwnerContract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(OwnerContract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static OwnerContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new OwnerContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static OwnerContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new OwnerContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class MonitoredValueEventEventResponse {
        public Uint256 _time;

        public Int256 _value;
    }

    public static class DRSignalRegistrationEventResponse {
        public DynamicArray<Int256> Erequest;
    }

    public static class ImbalanceRegistrationEventResponse {
        public DynamicArray<Int256> imbalance;
    }
}
