package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.crypto.CipherException;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import rx.Subscription;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

/**
 * Created by Claudia-PC on 6/6/2017.
 */
public class JsonRPCAnalysis {


    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException, CipherException {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101/"));  // defaults to http://localhost:8545/
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);


//        Subscription subscription = web3j.catchUpToLatestAndSubscribeToNewBlocksObservable(DefaultBlockParameter.valueOf(BigInteger.valueOf(0)),true).subscribe(block -> {
//           if(block.getError()!=null) {
//               System.out.println("ID: " + block.getId());
//               System.out.println("Error: " + block.getError());
//               System.out.println("Result: " + block.getResult());
//           }
//            for(EthBlock.TransactionResult t: block.getBlock().getTransactions()){
//            System.out.println("TX: "+ t.get());
//            }
//        });

//        Subscription subscription = web3j.catchUpToLatestAndSubscribeToNewTransactionsObservable(DefaultBlockParameter.valueOf(BigInteger.valueOf(0))).subscribe(tx -> {
//            System.out.println("==============NEW TX===============");
//            System.out.println("Block: " + tx.getBlockNumber());
//            System.out.println("gas price: " + tx.getGasPrice());
//            System.out.println("gas: " + tx.getGas());
//            System.out.println("From: " + tx.getFrom());
//            System.out.println("To: " + tx.getTo());
//            System.out.println("Creates: " + tx.getCreates());
//            System.out.println("Raw: " + tx.getRaw());
//        });



        org.web3j.protocol.core.methods.request.EthFilter ethFilter = new org.web3j.protocol.core.methods.request.EthFilter(
                DefaultBlockParameterName.EARLIEST,
                DefaultBlockParameterName.LATEST,
                "0x0ef15bb4e2026309afc786d34bd5ec7b6e72720c"
        );


        web3j.ethLogObservable(ethFilter).subscribe(log -> {
            System.out.println(log);
        });


    }

    }
