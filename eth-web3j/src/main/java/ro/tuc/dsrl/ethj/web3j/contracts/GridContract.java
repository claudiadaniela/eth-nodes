package ro.tuc.dsrl.ethj.web3j.contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * Auto generated code.<br>
 * <strong>Do not modify!</strong><br>
 * Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>, or {@link org.web3j.codegen.SolidityFunctionWrapperGenerator} to update.
 *
 * <p>Generated with web3j version 2.2.2.
 */
public final class GridContract extends Contract {
    private static final String BINARY = "60606040526018604051805910620000145750595b908082528060200260200182016040525b5080516200003c91600091602090910190620000f7565b5060186040518059106200004d5750595b908082528060200260200182016040525b5080516200007591600191602090910190620000f7565b506018604051805910620000865750595b908082528060200260200182016040525b508051620000ae91600291602090910190620000f7565b506018604051805910620000bf5750595b908082528060200260200182016040525b508051620000e791600391602090910190620000f7565b503415620000f157fe5b6200016c565b82805482825590600052602060002090810192821562000135579160200282015b828111156200013557825182559160200191906001019062000118565b5b506200014492915062000148565b5090565b6200016991905b808211156200014457600081556001016200014f565b5090565b90565b6112c0806200017c6000396000f3006060604052361561010f5763ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663032af1168114610111578063349b32e514610136578063385e7537146101a1578063467180f6146101f657806354bcddf91461020e5780635c81fedc14610279578063631392471461029e5780637af5dee6146102c35780637bbfb27e146102eb578063802e2bfa146103a0578063a1c3b983146103c8578063ae7c395e1461041d578063b4f642c814610479578063c058bcc1146104d5578063c352710c1461052a578063cab0f3351461053c578063cf7f8e42146105a7578063d07ca964146105fc578063e2ec526d14610721578063e4def7001461078c575bfe5b341561011957fe5b6101246004356107b1565b60408051918252519081900360200190f35b341561013e57fe5b6101466107d4565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b34156101a957fe5b6101f460048080359060200190820180359060200190808060200260200160405190810160405280939291908181526020018383602002808284375094965061083395505050505050565b005b34156101fe57fe5b6101f460043560243561088a565b005b341561021657fe5b6101466108b5565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b341561028157fe5b610124600435610914565b60408051918252519081900360200190f35b34156102a657fe5b610124600435610937565b60408051918252519081900360200190f35b34156102cb57fe5b61012460043560243561095a565b60408051918252519081900360200190f35b34156102f357fe5b6101466004808035906020019082018035906020019080806020026020016040519081016040528093929190818152602001838360200280828437509496505084359460200135935061099b92505050565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b34156103a857fe5b610124600435602435610a4a565b60408051918252519081900360200190f35b34156103d057fe5b6101f4600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750949650610b0695505050505050565b005b341561042557fe5b6101f460048080359060200190820180359060200190808060200260200160405190810160405280939291908181526020018383602002808284375094965050843594602001359350610b6992505050565b005b341561048157fe5b6101f460048080359060200190820180359060200190808060200260200160405190810160405280939291908181526020018383602002808284375094965050843594602001359350610c8092505050565b005b34156104dd57fe5b6101f4600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750949650610db595505050505050565b005b341561053257fe5b6101f4610e18565b005b341561054457fe5b610146610e53565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b34156105af57fe5b6101f4600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750949650610eb295505050505050565b005b341561060457fe5b610146600480803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750506040805187358901803560208181028481018201909552818452989a998901989297509082019550935083925085019084908082843750506040805187358901803560208181028481018201909552818452989a99890198929750908201955093508392508501908490808284375094965050843594602001359350610f1592505050565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b341561072957fe5b6101466111a1565b604080516020808252835181830152835191928392908301918581019102808383821561018e575b80518252602083111561018e57601f19909201916020918201910161016e565b5050509050019250505060405180910390f35b341561079457fe5b610124600435611200565b60408051918252519081900360200190f35b60018054829081106107bf57fe5b906000526020600020900160005b5054905081565b6107dc611282565b600180548060200260200160405190810160405280929190818152602001828054801561082857602002820191906000526020600020905b815481526020019060010190808311610814575b505050505090505b90565b60005b815181101561088557818181518110151561084d57fe5b9060200190602002015160038281548110151561086657fe5b906000526020600020900160005b50805490910190555b600101610836565b5b5050565b8160038281548110151561089a57fe5b906000526020600020900160005b50805490910190555b5050565b6108bd611282565b600380548060200260200160405190810160405280929190818152602001828054801561082857602002820191906000526020600020905b815481526020019060010190808311610814575b505050505090505b90565b60038054829081106107bf57fe5b906000526020600020900160005b5054905081565b60028054829081106107bf57fe5b906000526020600020900160005b5054905081565b600060006109688484610a4a565b90508060038481548110151561097a57fe5b906000526020600020900160005b50805490910190559050805b5092915050565b6109a3611282565b6109ab611282565b6000600086516040518059106109be5750595b908082528060200260200182016040525b508751909350868603146109e557829350610a40565b5060009050845b84811015610a3c57610a158783815181101515610a0557fe5b9060200190602002015182610a4a565b8383815181101515610a2357fe5b602090810290910101526001909101905b6001016109ec565b8293505b5050509392505050565b60038054600091829184908110610a5d57fe5b906000526020600020900160005b505415801590610aa95750610a9d600384815481101515610a8857fe5b906000526020600020900160005b5054611223565b610aa685611223565b14155b15610af757610ab784611223565b610aef610ac38661124f565b610aea600387815481101515610ad557fe5b906000526020600020900160005b505461124f565b611269565b029050610afb565b5060005b8091505b5092915050565b6000601882511115610b1757610885565b5060005b8151811015610885578181815181101515610b3257fe5b90602001906020020151600282815481101515610b4b57fe5b906000526020600020900160005b50555b600101610b1b565b5b5050565b6000825b82811015610bc2578482815181101515610b8357fe5b90602001906020020151600382815481101515610b9c57fe5b906000526020600020900160005b5080549091019055600191909101905b600101610b6d565b7fc7b66312a55d111ea4a79735dfad7a915cb0f9269a0977a28e80955cb302669c3286604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825283818151815260200191508051906020019060200280838360008314610c65575b805182526020831115610c6557601f199092019160209182019101610c45565b505050905001935050505060405180910390a15b5050505050565b610c88611282565b60006000610c9786868661099b565b9250600091508490505b83811015610cf6578282815181101515610cb757fe5b90602001906020020151600382815481101515610cd057fe5b906000526020600020900160005b5080549091019055600191909101905b600101610ca1565b7f475f1453a144fe44ac190ec6255a63d646a0249d1053eb65b186b1b1fcf478883284604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825283818151815260200191508051906020019060200280838360008314610d99575b805182526020831115610d9957601f199092019160209182019101610d79565b505050905001935050505060405180910390a15b505050505050565b6000601882511115610dc657610885565b5060005b8151811015610885578181815181101515610de157fe5b90602001906020020151600182815481101515610dfa57fe5b906000526020600020900160005b50555b600101610dca565b5b5050565b60005b600354811015610e4f576000600382815481101515610e3657fe5b906000526020600020900160005b50555b600101610e1b565b5b50565b610e5b611282565b600280548060200260200160405190810160405280929190818152602001828054801561082857602002820191906000526020600020905b815481526020019060010190808311610814575b505050505090505b90565b6000601882511115610ec357610885565b5060005b8151811015610885578181815181101515610ede57fe5b90602001906020020151600082815481101515610ef757fe5b906000526020600020900160005b50555b600101610ec7565b5b5050565b610f1d611282565b6000610f27611282565b610f2f611282565b610f37611282565b610f3f611282565b6000888803955085604051805910610f545750595b908082528060200260200182016040525b50945085604051805910610f765750595b908082528060200260200182016040525b50935085604051805910610f985750595b908082528060200260200182016040525b50925085604051805910610fba5750595b908082528060200260200182016040525b509150600090505b8581101561118e578a81815181101515610fe957fe5b906020019060200201518a8281518110151561100157fe5b9060200190602002015103858281518110151561101a57fe5b60209081029091010152600080548290811061103257fe5b906000526020600020900160005b505461107a868381518110151561105357fe5b906020019060200201518e8481518110151561106b57fe5b90602001906020020151611269565b02848281518110151561108957fe5b6020908102909101015260018054829081106110a157fe5b906000526020600020900160005b50546110ea8d838151811015156110c257fe5b9060200190602002015187848151811015156110da57fe5b906020019060200201510361124f565b0283828151811015156110f957fe5b60209081029091010152825183908290811061111157fe5b90602001906020020151848281518110151561112957fe5b9060200190602002015160028381548110151561114257fe5b906000526020600020900160005b50548c8481518110151561116057fe5b90602001906020020151020301828281518110151561117b57fe5b602090810290910101525b600101610fd3565b8196505b50505050505095945050505050565b6111a9611282565b600080548060200260200160405190810160405280929190818152602001828054801561082857602002820191906000526020600020905b815481526020019060010190808311610814575b505050505090505b90565b60008054829081106107bf57fe5b906000526020600020900160005b5054905081565b600080808312156112375750600019611244565b6000831315611244575060015b5b8091505b50919050565b6000818181121561124457600019025b8091505b50919050565b60008280831215610afb5750815b8091505b5092915050565b604080516020810190915260008152905600a165627a7a72305820286c3305e51e36e0592379c1a4f4e7dddb4005ba330f628184dc34c14ab9415d0029";

    private GridContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private GridContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<DRSignalRegistrationEventResponse> getDRSignalRegistrationEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("DRSignalRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<DynamicArray<Int256>>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<DRSignalRegistrationEventResponse> responses = new ArrayList<DRSignalRegistrationEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            DRSignalRegistrationEventResponse typedResponse = new DRSignalRegistrationEventResponse();
            typedResponse.from = (Address) eventValues.getNonIndexedValues().get(0);
            typedResponse.Erequest = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(1);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<DRSignalRegistrationEventResponse> dRSignalRegistrationEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("DRSignalRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<DynamicArray<Int256>>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, DRSignalRegistrationEventResponse>() {
            @Override
            public DRSignalRegistrationEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                DRSignalRegistrationEventResponse typedResponse = new DRSignalRegistrationEventResponse();
                typedResponse.from = (Address) eventValues.getNonIndexedValues().get(0);
                typedResponse.Erequest = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(1);
                return typedResponse;
            }
        });
    }

    public List<ImbalanceRegistrationEventResponse> getImbalanceRegistrationEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("ImbalanceRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<DynamicArray<Int256>>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<ImbalanceRegistrationEventResponse> responses = new ArrayList<ImbalanceRegistrationEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            ImbalanceRegistrationEventResponse typedResponse = new ImbalanceRegistrationEventResponse();
            typedResponse.from = (Address) eventValues.getNonIndexedValues().get(0);
            typedResponse.imbalance = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(1);
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ImbalanceRegistrationEventResponse> imbalanceRegistrationEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("ImbalanceRegistration", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<DynamicArray<Int256>>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, ImbalanceRegistrationEventResponse>() {
            @Override
            public ImbalanceRegistrationEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                ImbalanceRegistrationEventResponse typedResponse = new ImbalanceRegistrationEventResponse();
                typedResponse.from = (Address) eventValues.getNonIndexedValues().get(0);
                typedResponse.imbalance = (DynamicArray<Int256>) eventValues.getNonIndexedValues().get(1);
                return typedResponse;
            }
        });
    }

    public Future<Int256> finePrice(Uint256 param0) {
        Function function = new Function("finePrice", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<DynamicArray<Int256>> getFinePrice() {
        Function function = new Function("getFinePrice", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> registerClientBaseline(DynamicArray<Int256> baseline) {
        Function function = new Function("registerClientBaseline", Arrays.<Type>asList(baseline), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> registerImbalance(Int256 value, Uint256 h) {
        Function function = new Function("registerImbalance", Arrays.<Type>asList(value, h), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> getImbalances() {
        Function function = new Function("getImbalances", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Int256> imbalancesProfile(Uint256 param0) {
        Function function = new Function("imbalancesProfile", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Int256> energyPrice(Uint256 param0) {
        Function function = new Function("energyPrice", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> registerDrRequest(Int256 value, Uint256 h) {
        Function function = new Function("registerDrRequest", Arrays.<Type>asList(value, h), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> verifyDrRequest(DynamicArray<Int256> Erequest, Uint256 h1, Uint256 h2) {
        Function function = new Function("verifyDrRequest", 
                Arrays.<Type>asList(Erequest, h1, h2), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Int256> verifyDrRequest(Int256 value, Uint256 h) {
        Function function = new Function("verifyDrRequest", 
                Arrays.<Type>asList(value, h), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> registerEnergyPrice(DynamicArray<Int256> price) {
        Function function = new Function("registerEnergyPrice", Arrays.<Type>asList(price), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> registerImbalance(DynamicArray<Int256> imbalance, Uint256 h1, Uint256 h2) {
        Function function = new Function("registerImbalance", Arrays.<Type>asList(imbalance, h1, h2), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> registerDrRequest(DynamicArray<Int256> Erequest, Uint256 h1, Uint256 h2) {
        Function function = new Function("registerDrRequest", Arrays.<Type>asList(Erequest, h1, h2), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> registerFinePrice(DynamicArray<Int256> price) {
        Function function = new Function("registerFinePrice", Arrays.<Type>asList(price), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<TransactionReceipt> resetBaseline() {
        Function function = new Function("resetBaseline", Arrays.<Type>asList(), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> getEnergyPrice() {
        Function function = new Function("getEnergyPrice", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<TransactionReceipt> registerIncentivePrice(DynamicArray<Int256> price) {
        Function function = new Function("registerIncentivePrice", Arrays.<Type>asList(price), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }

    public Future<DynamicArray<Int256>> computeEnergyPrice(DynamicArray<Int256> DRsignal, DynamicArray<Int256> Ebaseline, DynamicArray<Int256> Emonitored, Uint256 h1, Uint256 h2) {
        Function function = new Function("computeEnergyPrice", 
                Arrays.<Type>asList(DRsignal, Ebaseline, Emonitored, h1, h2), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<DynamicArray<Int256>> getIncentivePrice() {
        Function function = new Function("getIncentivePrice", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Int256>>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public Future<Int256> incentivePrice(Uint256 param0) {
        Function function = new Function("incentivePrice", 
                Arrays.<Type>asList(param0), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }

    public static Future<GridContract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(GridContract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static Future<GridContract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialWeiValue) {
        return deployAsync(GridContract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "", initialWeiValue);
    }

    public static GridContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new GridContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static GridContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new GridContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class DRSignalRegistrationEventResponse {
        public Address from;

        public DynamicArray<Int256> Erequest;
    }

    public static class ImbalanceRegistrationEventResponse {
        public Address from;

        public DynamicArray<Int256> imbalance;
    }
}
