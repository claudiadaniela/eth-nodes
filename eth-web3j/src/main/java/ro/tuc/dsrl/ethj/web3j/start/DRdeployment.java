package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Int8;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import ro.tuc.dsrl.ethj.web3j.contracts.OwnerContract;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Claudia-PC on 6/8/2017.
 */
public class DRdeployment {


    private Web3j web3j;
    private OwnerContract contract;

    public DRdeployment(Web3j web3j){
        this.web3j = web3j;

    }

    public void  loadContract( Credentials credentials, String ownerContractAddress){
        contract = OwnerContract.load(
                ownerContractAddress, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }
    public void deployContract( Credentials credentials)
            throws ExecutionException, InterruptedException, IOException {

        Future<OwnerContract> contractFuture = OwnerContract.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT, BigInteger.ZERO);

         contract =  contractFuture.get();

        System.out.println("=========CLIENT Contract ========");
        System.out.println("address: "+ contract.getContractAddress());
        System.out.println("gas price: "+ contract.getGasPrice());

    }

    public void monitorValue() throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.monitorValue(new Uint256(1), new Int256(9));
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("=========CLIENT monitorValue ========");
        System.out.println("TX monitorValue hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX monitorValue block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX monitorValue gas: "+incrementalTransaction.getGasUsed());

        checkMonitoredEvents( incrementalTransaction);

    }

    public void checkMonitoredEvents(  TransactionReceipt receipt ){
        List<OwnerContract.MonitoredValueEventEventResponse> events =contract.getMonitoredValueEventEvents(receipt);
        System.out.println("========= CLIENT events ========");
        for(OwnerContract.MonitoredValueEventEventResponse resp : events){
            System.out.println("getMonitoredValueEventEvents value: "+ resp._value.getValue());
        }
    }


    public void getValuesField() throws ExecutionException, InterruptedException {
        Future<List<Type>> futureValue= contract.values(new Uint256(1));
        List<Type> values=  futureValue.get();
        System.out.println("========= CLIENT values field ========");
        for(Type t : values) {
            System.out.println("Call public values field value : " + t.getValue());
        }
    }

    public void getBaseline() throws ExecutionException, InterruptedException {
        Future<DynamicArray<Int256>> futureValue= contract.getBaseline();
        DynamicArray<Int256> values=  futureValue.get();
        System.out.println("========= CLIENT baseline method constant ========");
        for(Type t : values.getValue()) {
            System.out.println("Call baseline constant method : " + t.getValue());
        }
    }

    public void updateBaseline() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(-10));
        values.add(new Int256(-11));
        values.add(new Int256(-12));
        values.add(new Int256(-13));

        DynamicArray<Int256> consumption = new DynamicArray<>(values);

        values = new ArrayList<>();
        values.add(new Int256(12));
        values.add(new Int256(9));
        values.add(new Int256(15));
        values.add(new Int256(20));

        DynamicArray<Int256> production = new DynamicArray<>(values);


        Future<TransactionReceipt> futureTranscationReceipt= contract.updateBaseline(consumption, production);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= CLIENT updateBaseline ========");
        System.out.println("TX registerDrRequest hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerDrRequest block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerDrRequest gas: "+incrementalTransaction.getGasUsed());


    }

    public void updateStateIntraDay() throws ExecutionException, InterruptedException {
        List<Int256> values = new ArrayList<>();
        values.add(new Int256(-12));
        values.add(new Int256(-11));
        values.add(new Int256(-12));
        values.add(new Int256(-13));

        DynamicArray<Int256> consumption = new DynamicArray<>(values);

        values = new ArrayList<>();
        values.add(new Int256(12));
        values.add(new Int256(10));
        values.add(new Int256(16));
        values.add(new Int256(19));

        DynamicArray<Int256> production = new DynamicArray<>(values);


        Uint256 h1 = new Uint256(0);
        Uint256 h2 = new Uint256(4);

        Future<TransactionReceipt> futureTranscationReceipt= contract.updateStateIntraDay(consumption, production, h1, h2);
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= CLIENT updateStateIntraDay ========");
        System.out.println("TX registerDrRequest hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX registerDrRequest block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX registerDrRequest gas: "+incrementalTransaction.getGasUsed());

        checkDRRequestEvents(incrementalTransaction);
        checkImbalanceEvents(incrementalTransaction);
    }

    public void checkDRRequestEvents(  TransactionReceipt receipt ){
        List<OwnerContract.DRSignalRegistrationEventResponse>events =contract.getDRSignalRegistrationEvents(receipt);
        System.out.println("========= CLIENT events ========");
        for(OwnerContract.DRSignalRegistrationEventResponse resp : events){
            System.out.println("registered drRequests    values: " );
            for(Int256 v: resp.Erequest.getValue()){
                System.out.print( v.getValue() + " ");
            }
            System.out.println();
        }
    }

    public void checkImbalanceEvents(  TransactionReceipt receipt ){
        List<OwnerContract.ImbalanceRegistrationEventResponse> events =contract.getImbalanceRegistrationEvents(receipt);
        System.out.println("========= CLIENT events ========");
        for(OwnerContract.ImbalanceRegistrationEventResponse resp : events){
            System.out.println("registered imbalances    values: " );
            for(Int256 v: resp.imbalance.getValue()){
                System.out.print( v.getValue() + " ");
            }
            System.out.println();
        }
    }

    public void init(String gridContractAddress) throws ExecutionException, InterruptedException {
        Future<TransactionReceipt> futureTranscationReceipt= contract.init(new Address(gridContractAddress));
        TransactionReceipt incrementalTransaction=  futureTranscationReceipt.get();
        System.out.println("========= DEVICE gridContractAddress ========");
        System.out.println("TX init hash : "+incrementalTransaction.getTransactionHash());
        System.out.println("TX init block: "+incrementalTransaction.getBlockNumber());
        System.out.println("TX init gas: "+incrementalTransaction.getGasUsed());
    }

    public String getContractAddres(){
        return contract.getContractAddress();
    }

}
