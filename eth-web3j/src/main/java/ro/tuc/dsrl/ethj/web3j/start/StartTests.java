package ro.tuc.dsrl.ethj.web3j.start;

import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.exceptions.TransactionTimeoutException;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

/**
 * Created by Claudia-PC on 6/9/2017.
 */
public class StartTests {

    private static String ownerContractAddress = "0x75e947971e63b3e435a2f4b92ccc43c09bca5ef0";
    private static String deviceContractAddress = "0x99ea12b55d482bd582b97743dd4a014a24a5161c";
    private static String gridContractAddress = "0xe1cd0a9bc456808c2c3465f735924b88ddc0b692";

    private static String miner = "0x3b7b795f097bc6f131a4ded575d0976fc6a6a83e"; //"aaa" node1
    private static String regular = "0x8be71dee421d4e272f5f69abde9de193d396a8ec";// "bbb" node2

    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException, CipherException {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101/"));  // defaults to http://localhost:8545/
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);
        Credentials credentials = WalletUtils.loadCredentials("aaa", "E:/projects/blockchain/gethCluster/node1/keystore/UTC--2017-06-06T13-54-07.614832900Z--3b7b795f097bc6f131a4ded575d0976fc6a6a83e");


        GridContractDeployment gridTests = new GridContractDeployment(web3j);
        gridTests.loadContract(credentials, gridContractAddress);
//        gridTests.deployContract(credentials);
//        String gridAddress = gridTests.getContractAddres();


        DRdeployment clientContractTests = new DRdeployment(web3j);
        clientContractTests.loadContract(credentials, ownerContractAddress);
//        clientContractTests.deployContract(credentials);
//        String clientAddress = clientContractTests.getContractAddres();
//        clientContractTests.init(gridAddress);

        DeviceContractDeployment deviceContractTests = new DeviceContractDeployment(web3j);
        deviceContractTests.loadContract(credentials, deviceContractAddress);

        clientContractTests.getValuesField();
        
//        deviceContractTests.deployContract(credentials);
//        deviceContractTests.init(clientAddress);
//        deviceContractTests.updateValue();
//
//        clientContractTests.updateBaseline();
//        clientContractTests.getBaseline();
//        clientContractTests.updateStateIntraDay();
//
//        gridTests.registerEnergyPrice();
//        gridTests.registerFinePrice();
//        gridTests.registerIncentivePrice();
//        gridTests.getGridImbalances();


//        //  gridTests.loadContract(credentials, gridContractAddress);
//        gridTests.resetBaseline();
//        gridTests.registerImbalance();
//        gridTests.verifyDrRequest();
//        gridTests.registerDRRequest();
//
//
//        gridTests.computePrice();

    }

    private static void sendFunds(Web3j web3j, Credentials credentials) {
        try {
            TransactionReceipt transactionReceipt = Transfer.sendFunds(
                    web3j, credentials, regular, BigDecimal.valueOf(1.0), Convert.Unit.ETHER);

            System.out.println("========= TX Send Funds========");
            System.out.println("block: " + transactionReceipt.getBlockHash());
            System.out.println("hash: " + transactionReceipt.getTransactionHash());
            System.out.println("from: " + transactionReceipt.getFrom());
            System.out.println("to: " + transactionReceipt.getTo());
        } catch (TransactionTimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
