0) deploy a cluster using geth (or other client having JSON-RPC interfaces)
1) compile .sol contract using solc.exe  : solc sample.sol --bin --abi --optimize -o compiled/
2) run web3j in order to provide Java wrappers over the contract  using: path\to\contract.bin path\to\contract.abi -o path\to\eth-web3j\src\main\java -p ro.tuc.dsrl.ethj.web3j.contracts
3) use the Java wrapper classes to execute calls and transactions using the contract
