package ro.tuc.dsrl.blockchain.ethj.basic;

import org.springframework.context.annotation.Bean;


public class BasicNodeConfig {
    private BasicNodeConfig() {
    }

    @Bean
    public BasicNode basicNode() {
        return new BasicNode();
    }
}

