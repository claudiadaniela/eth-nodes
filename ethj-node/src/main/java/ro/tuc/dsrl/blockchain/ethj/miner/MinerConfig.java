package ro.tuc.dsrl.blockchain.ethj.miner;

import com.typesafe.config.ConfigFactory;
import org.ethereum.config.SystemProperties;
import org.springframework.context.annotation.Bean;


public  class MinerConfig {

    private final String config =
                    "peer.listen.port = 30335 \n" +
                    "peer.privateKey = 6ef8da380c27cea8fdf7448340ea99e8e2268fc2950d79ed47cbf6f85dc977ec \n" +
                    "sync.enabled = false \n" +
                    "database.dir = sampleDB-1 \n" +
                    // when more than 1 miner exist on the network extraData helps to identify the block creator
                    "mine.extraDataHex = cccccccccccccccccccc \n" +
                    "mine.cpuMineThreads = 2 \n" +
                    "cache.flush.blocks = 1";

    @Bean
    public MinerNode node() {
        return new MinerNode();
    }

    /**
     * Instead of supplying properties via config file for the peer
     * we are substituting the corresponding bean which returns required
     * config for this instance.
     */
    @Bean
    public SystemProperties systemProperties() {
        SystemProperties props = new SystemProperties();
        props.overrideParams(ConfigFactory.parseString(config.replaceAll("'", "\"")));
        return props;
    }
}