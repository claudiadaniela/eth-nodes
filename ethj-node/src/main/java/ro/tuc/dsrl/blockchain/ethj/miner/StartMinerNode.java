package ro.tuc.dsrl.blockchain.ethj.miner;

import org.ethereum.facade.EthereumFactory;

/**
 * Created by Claudia-PC on 11/17/2016.
 */
public class StartMinerNode {
    public static void main(String[] args) throws Exception {
        System.out.println("Starting EthtereumJ miner instance!");
        EthereumFactory.createEthereum(MinerConfig.class);
    }
}
