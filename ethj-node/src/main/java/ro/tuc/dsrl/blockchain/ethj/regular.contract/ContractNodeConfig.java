package ro.tuc.dsrl.blockchain.ethj.regular.contract;

import com.typesafe.config.ConfigFactory;
import org.ethereum.config.SystemProperties;
import org.springframework.context.annotation.Bean;


public class ContractNodeConfig {
    private final String config =
            "peer.listen.port = 30338 \n" +
                    "peer.privateKey = 3ec771c31cac8c0dba77a69e503765701d3c2bb62435888d4ffa38fed60c556c \n" +
                    "peer.active = [" +
                    "    { url = 'enode://26ba1aadaf59d7607ad7f437146927d79e80312f026cfa635c6b2ccf2c5d3521f5812ca2beb3b295b14f97110e6448c1c7ff68f14c5328d43a3c62b44143e9b1@localhost:30335' }" +
                    "] \n" +
                    "sync.enabled = true \n" +
                    "database.dir = sampleDB-4 \n";

    @Bean
    public ContractNode node() {
        return new ContractNode();
    }

    /**
     * Instead of supplying properties via config file for the peer
     * we are substituting the corresponding bean which returns required
     * config for this instance.
     */
    @Bean
    public SystemProperties systemProperties() {
        SystemProperties props = new SystemProperties();
        props.overrideParams(ConfigFactory.parseString(config.replaceAll("'", "\"")));
        return props;
    }
}
