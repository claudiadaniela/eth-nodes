package ro.tuc.dsrl.blockchain.ethj.basic;


import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.annotation.PostConstruct;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.ethereum.config.SystemProperties;
import org.ethereum.core.Block;
import org.ethereum.core.TransactionReceipt;
import org.ethereum.facade.Ethereum;
import org.ethereum.listener.EthereumListener;
import org.ethereum.listener.EthereumListenerAdapter;
import org.ethereum.net.eth.message.StatusMessage;
import org.ethereum.net.rlpx.Node;
import org.ethereum.net.server.Channel;
import org.ethereum.util.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BasicNode implements Runnable {

    private static final ConsoleAppender stdoutAppender = (ConsoleAppender) LogManager.getRootLogger().getAppender("stdout");
    private String loggerName;
    protected Logger logger;
    @Autowired
    protected Ethereum ethereum;
    @Autowired
    protected SystemProperties config;
    private volatile long txCount;
    private volatile long gasSpent;
    private List<Node> nodesDiscovered;
    private Map<Node, StatusMessage> ethNodes;
    private List<Node> syncPeers;
    private Block bestBlock;
    private boolean synced;
    private boolean syncComplete;
    private EthereumListener listener;


    public BasicNode() {
        this("basic");
    }

    public BasicNode(String loggerName) {
        this.nodesDiscovered = new Vector();
        this.ethNodes = new Hashtable<Node, StatusMessage>();
        this.syncPeers = new Vector();
        this.bestBlock = null;
        this.synced = false;
        this.syncComplete = false;
        this.listener = new BasicNodeEthereumListener();
        this.loggerName = loggerName;
    }

    private void setupLogging() {
        LogManager.getRootLogger().removeAppender("stdout");
        ConsoleAppender appender = new ConsoleAppender(stdoutAppender.getLayout());
        appender.setName("stdout");
        appender.setThreshold(Level.ERROR);
        LogManager.getRootLogger().addAppender(appender);
        this.logger = LoggerFactory.getLogger(this.loggerName);
        LogManager.getLogger(this.loggerName).addAppender(stdoutAppender);
    }

   // private void removeErrorLogging() {
   //     LogManager.getRootLogger().removeAppender("stdout");
   // }

    @PostConstruct
    private void springInit() {
        this.setupLogging();
        this.ethereum.addListener(this.listener);
        this.logger.info("Sample component created. Listening for ethereum events...");
        (new Thread(this, "SampleWorkThread")).start();
    }

    public void run() {
        try {
            this.logger.info("Sample worker thread started.");
            if (this.config.peerDiscovery()) {
                this.waitForDiscovery();
            } else {
                this.logger.info("Peer discovery disabled. We should actively connect to another peers or wait for incoming connections");
            }

            this.waitForAvailablePeers();
            this.waitForSyncPeers();
            this.waitForFirstBlock();
            this.waitForSync();
            this.onSyncDone();
        } catch (Exception var2) {
            this.logger.error("Error occurred in Sample: ", var2);
        }

    }

    public void onSyncDone() throws Exception {
        this.logger.info("Monitoring new blocks in real-time...");
    }

    private void waitForDiscovery() throws Exception {
        this.logger.info("Waiting for nodes discovery...");
        int bootNodes = this.config.peerDiscoveryIPList().size() + 1;
        int cnt = 0;

        while (true) {
            Thread.sleep(cnt < 30 ? 300L : 5000L);
            if (this.nodesDiscovered.size() > bootNodes) {
                this.logger.info("[v] Discovery works, new nodes started being discovered.");
                return;
            }

            if (cnt >= 30) {
                this.logger.warn("Discovery keeps silence. Waiting more...");
            }

            if (cnt > 50) {
                this.logger.error("Looks like discovery failed, no nodes were found.\nPlease check your Firewall/NAT UDP protocol settings.\nYour IP interface was detected as " + this.config.bindIp() + ", please check " + "if this interface is correct, otherwise set it manually via \'peer.discovery.bind.ip\' option.");
                throw new RuntimeException("Discovery failed.");
            }

            ++cnt;
        }
    }

    private void waitForAvailablePeers() throws Exception {
        this.logger.info("Waiting for available Eth capable nodes...");
        int cnt = 0;

        while (true) {
            Thread.sleep(cnt < 30 ? 1000L : 5000L);
            if (this.ethNodes.size() > 0) {
                this.logger.info("[v] Available Eth nodes found.");
                return;
            }

            if (cnt >= 30) {
                this.logger.info("No Eth nodes found so far. Keep searching...");
            }

            if (cnt > 60) {
                this.logger.error("No eth capable nodes found. Logs need to be investigated.");
            }

            ++cnt;
        }
    }

    private void waitForSyncPeers() throws Exception {
        this.logger.info("Searching for peers to sync with...");
        int cnt = 0;

        while (true) {
            Thread.sleep(cnt < 30 ? 1000L : 5000L);
            if (this.syncPeers.size() > 0) {
                this.logger.info("[v] At least one sync peer found.");
                return;
            }

            if (cnt >= 30) {
                this.logger.info("No sync peers found so far. Keep searching...");
            }

            if (cnt > 60) {
                this.logger.error("No sync peers found. Logs need to be investigated.");
            }

            ++cnt;
        }
    }

    private void waitForFirstBlock() throws Exception {
        Block currentBest = this.ethereum.getBlockchain().getBestBlock();
        this.logger.info("Current BEST block: " + currentBest.getShortDescr());
        this.logger.info("Waiting for blocks start importing (may take a while)...");
        int cnt = 0;

        while (true) {
            Thread.sleep(cnt < 300 ? 1000L : 60000L);
            if (this.bestBlock != null && this.bestBlock.getNumber() > currentBest.getNumber()) {
                this.logger.info("[v] Blocks import started.");
                return;
            }

            if (cnt >= 300) {
                this.logger.info("Still no blocks. Be patient...");
            }

            if (cnt > 330) {
                this.logger.error("No blocks imported during a long period. Must be a problem, logs need to be investigated.");
            }

            ++cnt;
        }
    }

    private void waitForSync() throws Exception {
        this.logger.info("Waiting for the whole blockchain sync (will take up to several hours for the whole chain)...");

        while (true) {
            Thread.sleep(10000L);
            if (this.synced) {
                this.logger.info("[v] Sync complete! The best block: " + this.bestBlock.getShortDescr());
                this.syncComplete = true;
                return;
            }

            this.logger.info("Blockchain sync in progress. Last imported block: " + this.bestBlock.getShortDescr() + " (Total: txs: " + this.txCount + ", gas: " + this.gasSpent / 1000L + "k)");
            this.txCount = 0L;
            this.gasSpent = 0L;
        }
    }

    private class BasicNodeEthereumListener extends EthereumListenerAdapter {
        public void onSyncDone() {
            BasicNode.this.synced = true;
        }

        public void onNodeDiscovered(Node node) {
            if (BasicNode.this.nodesDiscovered.size() < 1000) {
                BasicNode.this.nodesDiscovered.add(node);
            }
        }

        public void onEthStatusUpdated(Channel channel, StatusMessage statusMessage) {
            BasicNode.this.ethNodes.put(channel.getNode(), statusMessage);
        }

        public void onPeerAddedToSyncPool(Channel peer) {
            BasicNode.this.syncPeers.add(peer.getNode());
        }

        public void onBlock(Block block, List<TransactionReceipt> receipts) {
            BasicNode.this.bestBlock = block;
            BasicNode.this.txCount = (long) receipts.size();

           for(TransactionReceipt receipt : receipts) {
                BasicNode.this.gasSpent = ByteUtil.byteArrayToLong(receipt.getGasUsed());
            }

            if (BasicNode.this.syncComplete) {
                BasicNode.this.logger.info("New block: " + block.getShortDescr());
            }

        }
    }

}