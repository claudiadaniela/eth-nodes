package ro.tuc.dsrl.blockchain.ethj.regular;

import org.ethereum.facade.EthereumFactory;

/**
 * Created by Claudia-PC on 11/17/2016.
 */
public class StartRegularNode {
    public static void main(String[] args) throws Exception {
        System.out.println("Starting EthtereumJ regular instance!");
        EthereumFactory.createEthereum(RegularConfig.class);
    }
}
