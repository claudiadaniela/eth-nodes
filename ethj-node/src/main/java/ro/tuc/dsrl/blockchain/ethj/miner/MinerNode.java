package ro.tuc.dsrl.blockchain.ethj.miner;

import org.ethereum.core.Block;
import org.ethereum.mine.Ethash;
import org.ethereum.mine.MinerListener;
import ro.tuc.dsrl.blockchain.ethj.basic.BasicNode;

/**
 * Miner bean, which just start a miner upon creation and prints miner events
 */
public class MinerNode extends BasicNode implements MinerListener {
    public MinerNode() {
        // peers need different loggers
        super("sampleMiner");
    }

    // overriding run() method since we don't need to wait for any discovery,
    // networking or sync events
    @Override
    public void run() {
        if (config.isMineFullDataset()) {
            logger.info("Generating Full Dataset (may take up to 10 min if not cached)...");
            // calling this just for indication of the dataset generation
            // basically this is not required
            Ethash ethash = Ethash.getForBlock(config, ethereum.getBlockchain().getBestBlock().getNumber());
            ethash.getFullDataset();
            logger.info("Full dataset generated (loaded).");
        }
        ethereum.getBlockMiner().addListener(this);
        ethereum.getBlockMiner().startMining();
    }


    public void miningStarted() {
        logger.info("Miner started");
    }

    public void miningStopped() {
        logger.info("Miner stopped");
    }

    public void blockMiningStarted(Block block) {
        logger.info("Start mining block: " + block.getShortDescr());
    }

    public void blockMined(Block block) {
        logger.info("Block mined! : \n" + block);
    }

    public void blockMiningCanceled(Block block) {
        logger.info("Cancel mining block: " + block.getShortDescr());
    }
}