package ro.tuc.dsrl.blockchain.ethj.basic;

import org.ethereum.facade.EthereumFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StartBasicNode {
    static final Logger sLogger = LoggerFactory.getLogger("init");

    public static void main(String[] args) throws Exception {
        sLogger.info("Starting Basic Node EthereumJ!");
        EthereumFactory.createEthereum(BasicNodeConfig.class);
    }
}
