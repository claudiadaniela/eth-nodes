package ro.tuc.dsrl.blockchain.ethj.regular.contract;

import org.ethereum.crypto.ECKey;
import org.ethereum.facade.EthereumFactory;
import org.spongycastle.util.encoders.Hex;
import ro.tuc.dsrl.blockchain.ethj.miner.MinerConfig;

import static org.ethereum.crypto.HashUtil.sha3;

/**
 * Created by Claudia-PC on 11/18/2016.
 */
public class StartContractNode {

    public static void main(String[] args){
//          byte[] senderPrivateKey = sha3("cow".getBytes());
//
//          byte[] senderAddress = ECKey.fromPrivate(senderPrivateKey).getAddress();
//
//        System.out.println(senderPrivateKey);
//        System.out.println(Hex.encode("cow".getBytes()));
//        System.out.println(Hex.toHexString("cow".getBytes()));
//        System.out.println(senderAddress);

        System.out.println("Starting EthtereumJ contract node instance!");
        EthereumFactory.createEthereum(ContractNodeConfig.class);
    }
}
