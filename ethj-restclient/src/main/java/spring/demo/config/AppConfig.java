package spring.demo.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import spring.demo.beans.EthereumBean;

import java.util.concurrent.Executors;

@Configuration
@ComponentScan(basePackages = "spring.demo")
public class AppConfig {
    @Bean
    EthereumBean ethereumBean() throws Exception {
        EthereumBean ethereumBean = new EthereumBean();
        Executors.newSingleThreadExecutor().
                submit(ethereumBean::start);

        return ethereumBean;
    }
}
