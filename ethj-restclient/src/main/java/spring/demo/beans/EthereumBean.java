package spring.demo.beans;

import org.ethereum.facade.Ethereum;
import org.ethereum.facade.EthereumFactory;
import spring.demo.node.RegularConfig;


public class EthereumBean {

    private Ethereum ethereum;

    public void start() {

        this.ethereum = EthereumFactory.createEthereum(RegularConfig.class);
        System.out.println("Initialized ethereum...." + ethereum);
    }

    public static void main(String[] args) {
        EthereumFactory.createEthereum(RegularConfig.class);
    }

    public String getBestBlock() {
        System.out.println("Get Best block: " + ethereum);
        return "" + ethereum.getBlockchain().getBestBlock().getNumber();
    }
}
