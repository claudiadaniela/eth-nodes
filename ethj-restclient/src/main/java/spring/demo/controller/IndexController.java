package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.beans.EthereumBean;


@CrossOrigin(maxAge = 3600)
@RestController
public class IndexController {

    @Autowired
    EthereumBean ethereumBean;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start() {
        return "Welcome to Ethj!";
    }


    @RequestMapping(value = "/bestBlock", method = RequestMethod.GET)
    @ResponseBody
    public String getBestBlock() {
        return ethereumBean.getBestBlock();
    }
}
